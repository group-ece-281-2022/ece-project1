#include <stdio.h>
#include <time.h>
#include <stdlib.h>



Randcard(int a){
    a = (rand() % 13) + 1;
    return a;
}
Randsuit(int b){
    b = rand() % 4;
    return b;
}



int main(void){
    
    srand(time(0));
    
	//First draw
    int numCard1;
    int Suit1;
	
	//Guess
	int guess;
	
	//Second draw
    int numCard2;
    int Suit2;
	
	//Calculations
	int card1Score;
	int card2Score;
	int totalScoreSum;
	int totalScoreAltSum1;
	int totalScoreAltSum2;
	int totalScoreDif;
	int totalScoreAltDif1;
	int totalScoreAltDif2;
	
    // numCard1 = (rand() % 13) + 1;
    // Suit1 = (rand() % 4);
    // numCard2 = (rand() % 13) + 1;
    // Suit2 = (rand() % 4);

    numCard1 = Randcard(1);
    Suit1 = Randsuit(1);
	
    if((numCard1 > 1) && (numCard1 < 11)){
        printf("Your first card is the %d of ", numCard1);
    
    }
    else if(numCard1 == 1){
        printf("Your first card is the Ace of ");
    }
    else if(numCard1 == 11){
        printf("Your first card is the Jack of ");
    }
    else if(numCard1 == 12){
        printf("Your first card is the Queen of ");
    }
    else if(numCard1 == 13){
        printf("Your first card is the King of ");
    }
    // Code that will output a random card number

    if(Suit1 == 0){
        printf("Diamonds");
    }

      if(Suit1 == 1){
        printf("Spades");
    }

      if(Suit1 == 2){
        printf("Clubs");
    }

      if(Suit1 == 3){
        printf("Hearts");
    }

    //Code that will output a random suit 
    printf("!\n");
    
	// Guess your number
	printf("Guess your number: ");
	scanf("%d", guess);
	printf("\n");
	
	//Print your guess
	printf("Your guess is %d. Is it right?\n", guess);
	
	// Draw your second card
	numCard2 = Randcard(1);
    Suit2 = Randsuit(1);


    if((numCard2 > 1) && (numCard2 < 11)){
        printf("Your second card is the %d of ", numCard1);
    
    }
    else if(numCard2 == 1){
        printf("Your second card is the Ace of ");
    }
    else if(numCard2 == 11){
        printf("Your second card is the Jack of ");
    }
    else if(numCard2 == 12){
        printf("Your second card is the Queen of ");
    }
    else if(numCard2 == 13){
        printf("Your second card is the King of ");
    }
    // Code that will output a random card number

    if(Suit2 == 0){
        printf("Diamonds");
    }

      if(Suit2 == 1){
        printf("Spades");
    }

      if(Suit2 == 2){
        printf("Clubs");
    }

      if(Suit2 == 3){
        printf("Hearts");
    }

    //Code that will output a random suit 
    printf("!\n");
	
	//Calculations - point value of special suits
	
	//Card one
	card1Score = numCard1;
	
	if(numCard1 == 10) {
		card1Score = 10;
	}
	if(numCard1 == 11) {
		card1Score = 10;
	}
	if(numCard1 == 12) {
		card1Score = 10;
	}
	if(numcard1 == 13) {
		card1Score = 10;
	}
	
	//Card two
	card2Score = numCard2;
	
	if(numCard2 == 10) {
		card2Score = 10;
	}
	if(numCard2 == 11) {
		card2Score = 10;
	}
	if(numCard2 == 12) {
		card2Score = 10;
	}
	if(numcard2 == 13) {
		card2Score = 10;
	}

	//Calculations - calculating the total point value
	
	//Sum
	totalScoreSum = card1Score + card2Score;
	
		//Card one ace case
		if(card1Score == 1) {
			totalScoreAltSum1 = 11 + card2Score;
		}
	
		//Card two ace case
		if(card2Score == 1) {
			totalScoreAltSum2 = card1Score + 11;
		}

	//Difference
	totalScoreDif = card1Score - card2Score;
	totalScoreDif = abs(totalScoreDif);
	
		//Card one ace case
		if(card1Score == 1) {
			totalScoreAltDif1 = 11 - card2Score;
			totalScoreAltDif1 = abs(totalScoreAltDif1);
		}
		
		//Card two ace case
		if(card2Score == 1) {
			totalScoreAltDif2 = card1Score - 11;
			totalScoreAltDif2 = abs(totalScoreAltDif2);
		}
	
	//Return win conditions
	if (totalScoreSum == guess) {
		printf("Congrats! You win -- your guess of %d matched up to the sum!\n", guess);
	}
	else if (totalScoreAltSum1 == guess) {
		printf("Congrats! You win -- your guess of %d matched up to the sum!\n", guess);
	}
	else if (totalScoreAltSum2 == guess) {
		printf("Congrats! You win -- your guess of %d matched up to the sum!\n", guess);
	}
	else if (totalScoreDif == guess) {
		printf("Congrats! You win -- your guess of %d matched up to the absolute value of the difference!\n", guess);
	}
	else if (totalScoreAltDif1 == guess) {
		printf("Congrats! You win -- your guess of %d matched up to the absolute value of the difference!\n", guess);
	}
	else if (totalScoreAltDif2 == guess) {
		printf("Congrats! You win -- your guess of %d matched up to the absolute value of the difference!\n", guess);
	}
	else {
		printf("Aww, your guess of %d was a little off the mark -- try again next time!\n", guess);
	}
return 0;

}